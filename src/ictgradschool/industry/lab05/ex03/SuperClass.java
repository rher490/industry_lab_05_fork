package ictgradschool.industry.lab05.ex03;

/**
 * Created by rher490 on 20/11/2017.
 */
public class SuperClass {
    public int x = 10;
    static int y = 10;

    SuperClass() {
        System.out.println("SuperClass constructor: " + x + " and" + y);
        x = y++;
        System.out.println("After x=y++; x = " + x + " and y = "+y);
        System.out.println();
    }

    public int foo() {
        return x;
    }

    public static int goo() {
        return y;
    }
}


