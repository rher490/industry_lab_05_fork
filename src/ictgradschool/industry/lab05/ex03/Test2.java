package ictgradschool.industry.lab05.ex03;

/**
 * Created by rher490 on 20/11/2017.
 */
public class Test2 extends SuperClass {
    static int x = 15;
    static int y = 15;
    int x2= 20;
    static int y2 = 20;

    Test2() {
        System.out.println("Test2 Constructor x2:" + x2 + " and y2: "+y2);
        System.out.println("SC x:"+x + " y:"+y);
        System.out.println();
        x2 = y2++;
        System.out.println("afterx2=y2++;\n x2:" + x2 + " and y2: "+y2);
        System.out.println("SC x:"+x + " y:"+y);
        System.out.println();
    }

    public int foo2() {
        return x2;
    }

    public static int goo2() {
        return y2;
    }

    public static int goo(){
        return y2;
    }

    public static void main(String[] args) {
        SuperClass s2 = new Test2();
        System.out.println("\nThe static Binding");
        System.out.println("S2.x = " + s2.x);
        System.out.println("S2.y = " + s2.y);
        System.out.println("S2.foo() = " + s2.foo());
        System.out.println("S2.goo2() = " + s2.goo());
    }


}


